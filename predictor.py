#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  predictor.py
#  
#  © VIB Switch Laboratory
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  © VIB Switch Laboratory
from sklearn.feature_selection import RFE,SelectKBest
from sklearn.preprocessing import StandardScaler,MinMaxScaler
from sklearn.feature_selection import chi2
from sklearn.model_selection import GroupKFold

from sklearn.model_selection import cross_val_predict,cross_val_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression

from sklearn.metrics import matthews_corrcoef

import numpy as np

import propensity as prop
import parser
from run_foldx import run_foldx

import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import matthews_corrcoef
import utils
import pickle,os,json,random,multiprocessing

from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.Polypeptide import PPBuilder
import warnings
from Bio import BiopythonWarning
warnings.simplefilter('ignore', BiopythonWarning)

globalcont=0

PLOT=True
if PLOT:
	import matplotlib.pylab as plt

TEMPLATES_LIST=["1yjp_1","1yjp_2","2kib_1","2kib_2","2m5n_2","2m5n_3","2m5n_4","2okz_a","2ol9","2omm_1","2omm_2","2omp","2omq","2on9","2ona","2onv_a","2onw","2y2a","2y3j","2y3k_1","2y3k_2","2y3k_3","2y3l_1","2y3l_2","2y3l_3","2y29","3dg1_a","3dg1_b","3dgj_1","3dgj_2","3fpo","3fth_1","3fth_2","3ftk_1","3ftk_2","3ftl_a_1","3ftl_a_2","3ftl_b_1","3ftl_b_2","3ftr_a","3ftr_b","3fva_a","3fva_b","3hyd_a_1","3hyd_a_2","3hyd_b_1","3hyd_b_2","3loz","3nhc","3nhd","3nve","3ow9","3ppd_a","3ppd_b","3pzz","3q2x_a","3q2x_b","3sgs","4nin_1","4nin_2","4nio_a_1","4nio_a_2","4nio_b_2","4nip_a_1","4nip_a_2","4nip_b_1","4nip_b_2","4np8","4r0p_a","4r0p_b","4r0u_a_1","4r0u_a_2","4r0u_b_1","4r0u_b_2","4r0w_a_1","4r0w_a_2","4r0w_b_2","4rik_a_2","4rik_a_3","4rik_a_4","4rik_b_1","4rik_b_2","4rik_b_3","4ril_a_2","4ril_a_3","4ril_a_4","4ril_b_3","4ril_b_4","4ril_b_5","4rp6_1","4rp6_2","4rp7_a","4rp7_b","4tut","4uby","4ubz","4w5l_1","4w5l_2","4w5m_1","4w5m_2","4w5p_1","4w5p_2","4w5y_1","4w67_1","4w71_1","4w71_2","4wbu","4wbv","4xfo","4znn_a_3","4znn_a_4","4znn_a_5","4znn_b_3","4znn_b_4","4znn_b_5","5e5v_1","5e5v_2","5e5x","5e5z","5e5c","6cb9_a","6cb9_b","6cew","5n9i","6cfh_4","5vos_a_3","5vos_a_4","5vos_a_5","5vos_b_5","5vos_b_6","5w52_5","5whp","5wia","5wiq_1","5wiq_2","5wkb","5wkd_a_1","5wkd_a_2","5wkd_b_1","5wkd_b_2"]
TEMPLATES_DIR = "dataset/templates/"
RESULTS_FOLDER = "results/models/"
CPU=multiprocessing.cpu_count()

def calculate_foldex(seq): 
	
	precalculated_dg = pickle.load(open("marshalled/precalculated_dg.m","r"))
	print len(precalculated_dg.keys())
	inp=[]
	ppb=PPBuilder()
	
	todo = []
	todo_hash = []
	
	for i in range(len(seq)):
		if not precalculated_dg.has_key(seq[i]):
			
			todo+=[seq[i]]
			todo_hash+=[i]
			
	print len(todo),"hexas to run with foldX"
	if len(todo)>0:
		for template in TEMPLATES_LIST:
			structure = PDBParser().get_structure(template, TEMPLATES_DIR+template+".pdb")
			wtseq = str(ppb.build_peptides(structure)[0].get_sequence()) # taking the seq of the first chain. Assuming homoaggregation
			inp+=[(TEMPLATES_DIR+template+".pdb",wtseq,todo,RESULTS_FOLDER)]
		
		if CPU!=1:
			p = multiprocessing.Pool(processes=CPU)
			dg = p.map(run_foldx, inp)
			
		else:
			dg=[]
			for i in inp:
				dg=run_foldx(i)
			
		for s in todo:
			xt=[]
			
			for t in range(len(TEMPLATES_LIST)):
				xt+=[dg[t][s]]
				
			best_template=TEMPLATES_LIST[xt.index(min(xt))]
			for i in os.listdir(RESULTS_FOLDER):
				
				if not s == i[:6]:
					continue
				else:
					if not i.replace(".pdb","")[7:] == best_template:
						os.system("rm "+RESULTS_FOLDER+i)
					
			assert len(xt)==140
			precalculated_dg[s]=xt
			
		pickle.dump(precalculated_dg,open("marshalled/precalculated_dg.m","w"))
	
	final_140 = []
	
	for i in seq:	
		final_140+=[precalculated_dg[i]]
	
	return final_140
	
class Predictor():
	
	def __init__(self):		
		
		self.model_name='logreg'
		self.model=LogisticRegression(fit_intercept=True,class_weight='balanced')
	
	def fit(self,X,Y,print_coef=False,marshal_model=False):
		
		self.model.fit(X,Y)
		
		if marshal_model:
			pickle.dump(self.model,open('marshalled/model.m','w'))
			
	def predict(self,X):
		return self.model.predict_proba(X)[:,1]	
		
	def predict_proba(self,X):
		return self.model.predict_proba(X)
		
	def fit_best_threshold(self,ypred,y):

		best_mcc=-99
		for i in np.linspace(1,99):
			treshold=np.percentile(ypred[:],i)
			sen,spe,acc,pre,mcc,aucScore=utils.getscores(ypred[:], y[:],treshold,FULL_SCORES=True)
			
			if mcc>best_mcc:
				
				best_mcc=mcc
				best_t=treshold
				
		self.threshold=best_t
		return best_t		
		
	def get_params(self,deep=False):
		return {}
		
	def build_vector_peptides(self,seqs):
		v=calculate_foldex(seqs)
		return v
		
	def predict_seq(self,seq,outfile):
		p=[]
		for i in range(0,len(seq)-5,1):
			p+=[seq[i:i+6]]
			
		
		ypred=self.predict_hexas(p)
		v=[]
		for i in range(-5,len(ypred)):
			if i<0:
				iniz = 0
				fine=i+6
			elif i+6>=len(ypred):
				iniz=i
				fine = len(ypred)
			else:
				iniz=i
				fine = i+6
			v+=[max(ypred[iniz:fine])]
		ypred=v
		f=open(outfile+".csv","w")
		threshold=0.6106299122287593
		for i in range(len(seq)):
			if ypred[i]>threshold:
				binary="Amyloid"
			else:
				binary="NOT Amyloid"
			f.write(seq[i]+"\t"+str(ypred[i])+"\t"+binary+"\n")
		if PLOT:
			plt.plot(range(len(ypred)),ypred)
			plt.ylabel("Aggregation Prop")
			plt.xlabel("Sequence")
			plt.title(outfile.split("/")[-1])
			plt.savefig(outfile+".png")
			plt.clf()
		#return f

	def predict_hexas(self,seqs,outfile=False):
		
		x=[]
		y=[]
		salto=0
		g=[]
		s=[]
		
		x=self.build_vector_peptides(seqs)
		x=np.array(x)
		y=np.array(y)
		print "scaling"
		
		x=self.scaler.transform(x)
		x=self.rte.transform(x)
		ypred=self.predict(x)
		if not outfile:
			return ypred
			
		f=open(outfile,"w")
		threshold=0.6106299122287593
		for i in range(len(seqs)):
			
			if ypred[i]>threshold:
				binary="Amyloid"
			else:
				binary="NOT Amyloid"
			f.write(seqs[i]+"\t"+str(ypred[i])+"\t"+binary+"\n")
		return ypred

def filter_db_gruppi(db,maxsim=8,peplen=6):
	buone=[]
	print 'filtering...'
	rem_len=0
	rem=0
	groups=[]
	gru_lab={}
	seqfin=[]
	cont=0
	for i in range(len(db)):
		if len(db[i])!=peplen:
			rem_len+=1
			continue
		cattiva=False
		for j in gru_lab.keys():
			
			for p in gru_lab[j]:
				c=0
				for k in xrange(6):
					if db[i][k]==p[k]:
						c+=1
				if c>maxsim:
					cattiva=True
					groups+=[j]
					gru_lab[j]+=[db[i]]
					break
			if cattiva:
				break
		if not cattiva:
			groups+=[cont]
			gru_lab[cont]=[db[i]]
			cont+=1
		seqfin+=[db[i]]
	print 'Done, now there are ',len(seqfin),'entries. Removed:',rem

	return seqfin,groups
	
def fea_select(svc,x,y):
	rfe = RFE(estimator=svc, n_features_to_select=50, step=0.6)
	rfe.fit(x, y)
	ranking = rfe.ranking_
	selected=rfe.support_
	return rfe#.transform(x)
	
def get_new_model(validate=True):
	
	seqs,foldx=parser.parse_foldx()
	seqs,groups=filter_db_gruppi(seqs)
	
	model = Predictor()
	x=[]
	y=[]
	g=[]
	s=[]
	salto=0
	for i in range(len(seqs))[:]:
				
				y+=[foldx[i]]
				g+=[groups[i]]
				s+=[seqs[i]]


	x=model.build_vector_peptides(seqs)			

	x=np.array(x)
	y=np.array(y)
	g=np.array(g)
	
	print "scaling"
	scaler=StandardScaler()
	x=scaler.fit_transform(x)
	
	print "feature selection"
	rte=fea_select(LogisticRegression(fit_intercept=True,class_weight='balanced'),x,y)
	x=rte.transform(x)

	if not validate:
		
		print "fitting the final model"
		model.fit(x,y)
		model.scaler = scaler
		model.rte = rte
		print "fitting best threshold"
		gkf = GroupKFold(n_splits=max(g))
		ypred = cross_val_predict(Predictor(), x,y, cv=gkf,groups=g)
		model.fit_best_threshold(ypred,y)
		
		return model
	
	print 'starting CV'
	
	model.scaler = scaler
	model.rte = rte
	
	gkf = GroupKFold(n_splits=max(g))
	
	ypred=cross_val_predict(Predictor(), x,y, cv=gkf,groups=g)

	scores=open("waltzdb.csv","w")
	scores.write("seq\tprediction\tlabel\n")

	for i in range(len(ypred)):
		scores.write(s[i]+"\t"+str(ypred[i])+"\t"+str(y[i])+"\n")

	plot_roc(ypred,y)
	model.threshold = model.fit_best_threshold(ypred,y)
	print "best threshold", model.threshold
	
	sen,spe,acc,pre,mcc,aucScore=utils.getscores(ypred, y, model.threshold,FULL_SCORES=True)
	
	print 'model performances:'
	print '\tSEN',sen
	print '\tSPE',spe
	print '\tACC',acc
	print '\tPRE',pre
	print '\tMCC',mcc
	print '\tAUC',aucScore
	
	f=open("waltzdb.csv","w")
	for i in range(len(s)):
		f.write(s[i]+"\t"+str(ypred[i])+"\n")
	f.close()

def sliding_window_pep(x):
	ws=7
	v=[]
	for i in range(-2,len(x)+3):
		if i-ws+1<0:
			iniz=0
		else:
			iniz=i-ws+1
		if i+ws>len(x):
			fin=len(x)
		else:
			fin=i+ws
		if len(x[iniz:fin])==0:
			v+=[0.5]
		else:
			v+=[np.mean(x[iniz:fin])]
	return v
	
def plot_roc(x,y):
	from sklearn.metrics import roc_curve, auc
	import matplotlib.pylab as plt
	fpr, tpr, _ = roc_curve(y, x)
	plt.figure()
	lw = 2
	roc_auc=auc(fpr, tpr)
	plt.plot(fpr, tpr, color='darkorange',
			 lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
	plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
	plt.xlim([0.0, 1.0])
	plt.ylim([0.0, 1.05])
	plt.xlabel('False Positive Rate')
	plt.ylabel('True Positive Rate')
	plt.title('WaltzDB')
	plt.legend(loc="lower right")
	plt.savefig("roc.png",dpi=300)
	plt.show()	
	
if __name__ == '__main__':
	#plot_roc(np.random.randn(100),np.random.randint(0, high=2, size=100))
	get_new_model()
	
	import sys
	#model=get_new_model(validate=False)
	for i in os.listdir("dataset/Wetzel_scrambles/"):
		print '>'+i.replace(".txt","")+'\n'+i.replace(".txt","")

	
