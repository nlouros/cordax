#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  run_foldx.py
#  
#  © VIB Switch Laboratory
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  © VIB Switch Laboratory
import parser 
import random,string,os
letters={'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'GLN': 'Q', 'LYS': 'K', 'ASN': 'N', 'PRO': 'P', 'THR': 'T', 'PHE': 'F', 'ALA': 'A','H1S': 'H','H2S': 'H', 'HIS': 'H', 'GLY': 'G', 'ILE': 'I', 'LEU': 'L', 'ARG': 'R', 'TRP': 'W', 'VAL': 'V', 'GLU': 'E', 'TYR': 'Y', 'MET': 'M'}
ss8to3={'G':[1,0,0],'H':[1,0,0],'I':[1,0,0],'E':[0,0,1],'B':[0,0,1],'S':[0,1,0],'T':[0,1,0],'C':[0,1,0],'-':[0,1,0],'*':[0,1,0],'n':[0,1,0],'a':[1,0,0],'c':[0,1,0]}

FOLDX_FOLDER='./foldx/'

def write_mutfile(seqs,tmp_folder):
	name=''.join(random.choice(string.ascii_lowercase) for i in range(5))
	f=open(tmp_folder+'mutant_file_'+name+'.tmp','w')
	for i in seqs:
		f.write(i+'\n')
	f.close()
	return name

def parse_buildmodel_energy(fil):
	diz={}
	for l in open(fil,"r").readlines():
		a=l.split("\t")
		if ".pdb" in a[0]:
			name=a[0].replace(".pdb","").split("_")[-1]
			diz[name]=float(a[1])
	return diz
	
def run_foldx(inp, tmp_folder='foldx_TMP/'):
	pdb_target,wt_seq, mut_seqs, model_outdir=inp
	
	dafare=mut_seqs
	seqs=[wt_seq]+mut_seqs
	
	os.popen('mkdir -p '+ tmp_folder)
	os.popen('mkdir -p '+ model_outdir)
	

	name=write_mutfile(seqs,tmp_folder)

	if '/' in pdb_target:
		pdb_name=pdb_target.split('/')[-1]
		pdb_fold='/'.join(pdb_target.split('/')[:-1])

	os.mkdir(tmp_folder+name)

	os.popen(FOLDX_FOLDER+'foldx --pdb-dir='+pdb_fold+' --rotabaseLocation='+FOLDX_FOLDER+'rotabase.txt --pdb='+pdb_name+' -c BuildModel --mutant-file='+tmp_folder+'mutant_file_'+name+'.tmp --output-dir='+tmp_folder+name)
	os.popen('rm '+tmp_folder+'mutant_file_'+name+'.tmp')
	foldx_outfile = tmp_folder+name+"/Raw_"+pdb_name.replace(".pdb",".fxout")
	energy = parse_buildmodel_energy(foldx_outfile)
	fin={}
	
	for i in range(1,len(seqs)):
		model_name=pdb_name.replace(".pdb","")+"_"+str(i)+".pdb"
		os.system("cp "+tmp_folder+name+"/"+model_name+" "+model_outdir+seqs[i]+"_"+pdb_name)
		fin[seqs[i]]=energy[str(i)]

	os.system('rm -r '+tmp_folder+name )
	return fin
			
def main(args):
    return 0

if __name__ == '__main__':
    import sys
    a=run_foldx('/home/scimmia/Desktop/programmi/peptided_foldx_cons_git/dataset/templates/3ftr_a.pdb',"SSTNVG",{'CACCA':'CACCAC',"BACCA":"SDCVRT"},'A')
    print a['CACCAC']
