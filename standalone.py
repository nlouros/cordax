#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  standalone.py
#  
#  © VIB Switch Laboratory
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  © VIB Switch Laboratory
import warnings
warnings.filterwarnings("ignore")
import argparse,pickle,os,multiprocessing
from predictor import Predictor, get_new_model

def run(args):
	args=args[1:]
	pa = argparse.ArgumentParser()
	
	
	
	pa.add_argument('-i', '--infile',
						help='the input fasta file',
						)
	pa.add_argument('-fit','--fit',
						help='re-runs the training and saves the model',
						action='store_true',
						default=False
						)
	pa.add_argument('-o', '--outfile',
						help='output file',
						default="results/")
	
	'''					
	pa.add_argument('-c', '--cpu',
						help='number of cpu to use',
						default=multiprocessing.cpu_count())
	'''	
										
	pa.add_argument('-fullseq', '--fullseq',
						help='run the model for full length proteins. The sequence will be chopped in hexapeptides',
						action='store_true',
						default=False)
						
	results = pa.parse_args(args)
	
	#global CPU
	#CPU = results.cpu
	
	if results.fit:
		model = get_new_model(validate=False)
		pickle.dump(model,open('marshalled/model.m',"w"))
		print '############################################'
		print '            TRAIN SUCCESSFUL!'
		print '############################################'
		print 'CORDAX is re-trained!'
		return
	else:
		try:
			model = pickle.load(open('marshalled/model.m',"r"))
		except:
			print "problems with the serialized model, try python2 standalone.py --fit"
	seqs=leggifasta(results.infile)
	if not results.fullseq:
		print "running in hexa-mode"
		for i in seqs.keys():
			if len(seqs[i])!=6:
				print "sequence ",i,"is not an hexapeptide. If you want to run in fullseq mode, use the --fullseq option"
				return
		model.predict_hexas(seqs.values(),outfile=results.outfile)
		print "DONE!"
	else:
		print "running fullseq mode"
		os.system("mkdir -p "+results.outfile )
		for i in seqs.keys():
			model.predict_seq(seqs[i],outfile=results.outfile+"/"+i)
		print "DONE!"
		
def leggifasta(database): 
		f=open(database)
		uniprot=f.readlines()
		f.close()
		dizio={}
		for i in uniprot:

			if i[0]=='>':
					if '|' in i:
						uniprotid=i.strip('>\n').split('|')[1]
					else:
						uniprotid=i.strip('>\n')
					dizio[uniprotid]=''
			else:
				dizio[uniprotid]=dizio[uniprotid]+i.strip('\n\r')
		return dizio
		
def main(args):
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(run(sys.argv))
