#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  utils.py
#  
#  © VIB Switch Laboratory
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  © VIB Switch Laboratory
aa=['A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W','Y']
import matplotlib.pyplot as plt
import numpy as np
def getscores(pred, real, threshold,FULL_SCORES=True):
	import math
	import numpy as np
	if len(pred) != len(real):
		raise Exception("ERROR: input vectors have differente len!")
	i = 0
	confusionMatrix = {}
	confusionMatrix["TP"] = confusionMatrix.get("TP", 0)
	confusionMatrix["FP"] = confusionMatrix.get("FP", 0)
	confusionMatrix["FN"] = confusionMatrix.get("FN", 0)
	confusionMatrix["TN"] = confusionMatrix.get("TN", 0)
	while i < len(real):
		if float(pred[i])<=threshold and (int(real[i])==0):
			confusionMatrix["TN"] = confusionMatrix.get("TN", 0) + 1
		if float(pred[i])<=threshold and int(real[i])==1:
			confusionMatrix["FN"] = confusionMatrix.get("FN", 0) + 1
		if float(pred[i])>=threshold and int(real[i])==1:
			confusionMatrix["TP"] = confusionMatrix.get("TP", 0) + 1
		if float(pred[i])>=threshold and int(real[i])==0:
			confusionMatrix["FP"] = confusionMatrix.get("FP", 0) + 1
		i += 1
	#print confusionMatrix
	if FULL_SCORES:
	
		#print "--------------------------------------------"
		#print confusionMatrix["TN"],confusionMatrix["FN"],confusionMatrix["TP"],confusionMatrix["FP"]
		#print "          | SSBOND            | FREE             |"
		#print "predBond  | TP: %d (%2.2f%%)  | FP: %d (%2.2f%%) |" % (confusionMatrix["TP"], (confusionMatrix["TP"]/float(confusionMatrix["TP"]+confusionMatrix["FN"]))*100, confusionMatrix["FP"], (confusionMatrix["FP"]/float(confusionMatrix["FP"]+confusionMatrix["TN"]))*100 )
		#print "predFree  | FN: %d (%2.2f%%)  | TN: %d (%2.2f%%) |" % (confusionMatrix["FN"],(confusionMatrix["FN"]/float(confusionMatrix["TP"]+confusionMatrix["FN"]))*100, confusionMatrix["TN"], (confusionMatrix["TN"]/float(confusionMatrix["FP"]+confusionMatrix["TN"]))*100)
		sen = (confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FN"])))
		spe = (confusionMatrix["TN"]/float((confusionMatrix["TN"] + confusionMatrix["FP"])))
		acc =  (confusionMatrix["TP"] + confusionMatrix["TN"])/float((sum(confusionMatrix.values())))
		bac = (0.5*((confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/float((confusionMatrix["TN"] + confusionMatrix["FP"])))))
		#inf =((confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/float((confusionMatrix["TN"] + confusionMatrix["FN"])))-1.0)
		pre =(confusionMatrix["TP"]/float((confusionMatrix["TP"] + confusionMatrix["FP"])))
		mcc =	( ((confusionMatrix["TP"] * confusionMatrix["TN"])-(confusionMatrix["FN"] * confusionMatrix["FP"])) / math.sqrt((confusionMatrix["TP"]+confusionMatrix["FP"])*(confusionMatrix["TP"]+confusionMatrix["FN"])*(confusionMatrix["TN"]+confusionMatrix["FP"])*(confusionMatrix["TN"]+confusionMatrix["FN"])) )  
	
	#print real
	#print pred
	from sklearn.metrics import roc_auc_score
	aucScore = roc_auc_score(real, pred)
	
	#print "AUC = %3.3f" % aucScore
	#print "--------------------------------------------"
	if FULL_SCORES:
		return sen,spe,acc,pre,mcc,aucScore
	else:
		return aucScore
def plot_heatmap(a):
	
	## filtrs
	for i in range(len(a)):
		for j in range(len(a[0])):
			if a[i][j]>1:
				a[i][j]=1
			elif a[i][j]<-1:
				a[i][j]=-1
	plt.imshow(a, cmap='seismic', interpolation='nearest')
	plt.xticks(range(20),aa)
	plt.yticks(range(6))
	plt.colorbar()
	#plt.show()
def plot_roc_curve(y_test, y_score):
	from sklearn.metrics import precision_recall_curve
	import matplotlib.pyplot as plt
	from sklearn.utils.fixes import signature
	from sklearn.metrics import roc_curve, auc
	from sklearn import svm, datasets
	from sklearn.model_selection import train_test_split
	import numpy as np
	precision, recall, _ = precision_recall_curve(y_test, y_score)

	# In matplotlib < 1.5, plt.fill_between does not have a 'step' argument
	step_kwargs = ({'step': 'post'}
				   if 'step' in signature(plt.fill_between).parameters
				   else {})
	plt.step(recall, precision, color='b', alpha=0.2,
			 where='post')
	plt.fill_between(recall, precision, alpha=0.2, color='red', **step_kwargs)

	plt.xlabel('Sensitivity')
	plt.ylabel('Precision')
	plt.ylim([0.0, 1.05])
	plt.xlim([0.0, 1.0])
	plt.title('Precision-Recall curve')
	plt.show()

	fpr, tpr, _ = roc_curve(y_test, y_score)

	step_kwargs = ({'step': 'post'}
				   if 'step' in signature(plt.fill_between).parameters
				   else {})
	plt.step(fpr, tpr, color='b', alpha=0.2,
			 where='post')
	plt.fill_between(fpr, tpr, alpha=0.2, color='red', **step_kwargs)

	plt.xlabel('Sensitivity')
	plt.ylabel('False Positive Rate')
	plt.ylim([0.0, 1.05])
	plt.xlim([0.0, 1.0])
	plt.title('ROC curve')
	plt.show()

def coef_rf(rf,x):
	#prediction, biases, contributions = ti.predict(rf, np.array(range(20)))
	prediction, bias, contributions = ti.predict(rf, x)
	coef=np.mean(contributions,axis=0)[:,1]
	coef_norm=np.reshape(coef,(6,20))
	coef_norm=np.interp(coef_norm, (coef_norm.min(), coef_norm.max()), (-1, +1))
	plot_heatmap(coef_norm)
	#asd
	
