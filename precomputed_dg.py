#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  precomputed_dg.py
#  
#  © VIB Switch Laboratory
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  © VIB Switch Laboratory
import os
def parser(fil):
	f=open(fil,"r").readlines()
	header=f[0].split("\t")
	diz={}
	cont=2
	for i in f[1:]:
		
		l=i.strip()
		if l=="":
			continue
		l=l.split("\t")
		fea=[]
		
		cont+=1
		for k in range(len(header)):
			
			if header[k].lower()=="sequence":
				name=l[k]
			elif header[k].lower()=="class":
				pass
			else:
				
				fea+=[float(l[k])]
		diz[name]=fea
		del name
	return diz	
	
def load_precomputed_dg(dir="dataset/precomputed_dg/"):
	dir="dataset/precomputed_dg/"
	import pickle
	diz={}
	for i in os.listdir(dir):
		diz.update(parser(dir+i))
	
	pickle.dump(diz,open("marshalled/precalculated_dg.m","w"))
	print len(diz.keys())
	return diz

if __name__ == '__main__':
	load_precomputed_dg()
