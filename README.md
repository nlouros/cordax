# CORDAX

CORDAX is a peptide aggregation propensity predictor based on predicted packing energies.  
It utilizes predefined information mined from an annotated library of amyloid fibril core crystal structures, combined with parameter fitting using logistic regression, in order to determine the free energy landscape and structural orientation of putative aggregation prone regions.

### Installation

CORDAX is implemented in python2.7. The current implementation supports Linux and Mac operative systems.
The following libreries are required :
  - FoldX: download both the foldX binary and "rotabase" from http://foldxsuite.crg.eu/. Put both files in cordax/foldx
  - sklearn  (it can be installed with "pip install sklearn")
  - biopython (it can be installed with "pip install biopython")
  - numpy (it can be installed with "pip install numpy")
  - matplotlib (it can be installed with "pip install matplotlib")


### Usage
From the CORDAX folder, run python standalone.py with the following options:
  
  - -h : shows help
  - -i : input FASTA file
  -  -fit : re-trains the model and saves it
  -  -o : output file
  -  --fullseq : Enables fullseq mode, allowing the prediction of proteins/peptides longer than 6 residues

### Examples

The "test" folder contains two fasta files, one with hexapeptides and one with longer fragments.
To test the fullseq mode run:
`python2 standalone.py -i test/test_proteins.fasta -o outdir --fullseq`
To test the peptides mode run:
`python2 standalone.py -i test/test_peptides.fasta -o outfile`

The models with the lowest deltaG are stored in results/models

